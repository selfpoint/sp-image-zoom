(function (angular, app) {
    'use strict';

    // save this amount of history, to not add too much memory
    // and still prevent trying to load an image too many times (usually recent images will get loaded again)
    var SAVED_LENGTH = 100;

    /**
     * Will try to load an image in the background and returns whether could be loaded or not
     * Will save a cache of recent history to make next attempts sync
     */
    function srv() {
        var self = this,
            _cache = [];

        self.load = load;

        function load(url, callback) {
            var imageStatus = _cache.find(function(i) {
                return i.url === url;
            });
            if (imageStatus) {
                _handleCacheImage(imageStatus, callback);
            } else {
                _loadImage(url, callback);
            }
        }

        function _handleCacheImage(imageStatus, callback) {
            if (imageStatus.isLoading) {
                imageStatus.callbacks.push(callback);
            } else {
                _callCallbacks(imageStatus, [function(value) {
                    // return true for 'isCache', to identify sync and async calls
                    callback(value, true);
                }]);
            }
        }

        function _loadImage(url, callback) {
            var imageStatus = {
                url: url,
                callbacks: [callback],
                isLoading: true
            };
            _pushCache(imageStatus);

            var image = new Image();
            image.onload = function() {
                imageStatus.isLoading = false;
                imageStatus.loaded = true;
                _callCallbacks(imageStatus);
            };
            image.onerror = function() {
                imageStatus.isLoading = false;
                imageStatus.loaded = false;
                _callCallbacks(imageStatus);
            };
            image.src = url;
        }

        function _pushCache(imageStatus) {
            _cache.unshift(imageStatus);
            _cache.splice(SAVED_LENGTH);
        }

        function _callCallbacks(imageStatus, callbacks) {
            callbacks = callbacks || imageStatus.callbacks;

            angular.forEach(callbacks, function(callback) {
                callback(imageStatus.loaded);
            });

            // these callbacks are not longer needed, they were called
            delete imageStatus.callbacks;
        }
    }

    /**
     * Register to Module
     */
    angular.module('spImageZoom').service('SpImagesLoader', [srv]);
})(angular);
