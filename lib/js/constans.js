(function (angular, app) {
    'use strict';

    app.constant('IMAGE_ZOOM_MODES', {
        NONE: 1,
        MAGNIFYING_GLASS: 2,
        ZOOM_CONTROLS: 3
    });
    app.constant('ZOOM_CONTROLS_ZOOM_OPTIONS', [1, 3, 6, 9]);

    app.run(['$rootScope', 'IMAGE_ZOOM_MODES', function($rootScope, IMAGE_ZOOM_MODES) {
        $rootScope.spImageZoom = $rootScope.spImageZoom || {};
        $rootScope.spImageZoom.IMAGE_ZOOM_MODES = IMAGE_ZOOM_MODES;
    }]);
})(angular, angular.module('spImageZoom'));
