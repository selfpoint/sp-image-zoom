(function (angular, app) {
    'use strict';

    var DRAG_AREA = '' +
        '<div class="drag-area"></div>';

    var CONTROL_BUTTONS = '' +
        '<div class="control-buttons">' +
        '<button class="zoom-in no-design" type="button" aria-label="Zoom In"></button>' +
        '<button class="zoom-out no-design" type="button" aria-label="Zoom Out"></button>' +
        '</div>';

    function ZoomControls($scope, $element, options) {
        options = options || {};

        var $document = angular.element(document),
            $dragAreaElement = angular.element(DRAG_AREA),
            $navigationControlsElement = angular.element(CONTROL_BUTTONS),
            _currentZoomIndex,
            _currentZoomValue,
            _translateLeft = 0,
            _translateTop = 0,
            _currentMovePoint,
            modeClass = 'zoom-controls-mode',
            isTouch = false;

        this.destroy = destroy;

        $element.addClass(modeClass);
        $element.append($dragAreaElement);
        $element.append($navigationControlsElement);

        $dragAreaElement.on('mousedown', _mouseDown);
        $dragAreaElement.on('touchstart', _touchStart);

        var $zoomInButton = angular.element($navigationControlsElement[0].getElementsByClassName('zoom-in')),
            $zoomOutButton = angular.element($navigationControlsElement[0].getElementsByClassName('zoom-out'));
        $zoomInButton.on('mousedown', _zoomInMouse);
        $zoomInButton.on('touchstart', _zoomInTouch);
        $zoomInButton.on('keydown', _enterKeyDownListener(_zoomIn));
        $zoomOutButton.on('mousedown', _zoomOutMouse);
        $zoomOutButton.on('touchstart', _zoomOutTouch);
        $zoomOutButton.on('keydown', _enterKeyDownListener(_zoomOut));

        var zoomOptions = [],
            defaultIndex;
        angular.forEach(options.zoom, function(zoomOption, index) {
            if (defaultIndex === undefined && zoomOption && zoomOption.isDefault) {
                defaultIndex = index;
            }

            zoomOptions.push(zoomOption.value || zoomOption);
        });
        _setCurrentZoom(defaultIndex || 0);

        function destroy() {
            delete $scope.calcImageUrl;
            $element.removeClass(modeClass);
            $dragAreaElement.remove();
            $navigationControlsElement.remove();
            _endMove();

            _getImageElement().css({ '-webkit-transform': '', '-ms-transform': '', 'transform': '' });
        }

        function _mouseDown(event) {
            if(!isTouch) {
                _startMoving(event.pageX, event.pageY);
            }
        }

        function _touchStart(event) {
            isTouch = true;
            _startMoving(event.touches[0].pageX, event.touches[0].pageY);
        }

        function _startMoving(startX, startY) {
            _currentMovePoint = {
                x: startX,
                y: startY
            };

            if(isTouch) {
                $document[0].addEventListener('touchmove', _touchMove, { passive: false }); //== have to send passive: false to use preventDefault
                $document.on('touchend', _endMove);
            } else {
                $document.on('mousemove', _mouseMove);
                $document.on('mouseup', _endMove);
            }
        }

        function _mouseMove(event) {
            event.preventDefault();
            _onMove(event.pageX, event.pageY);
        }

        function _touchMove(event) {
            event.preventDefault();
            _onMove(event.touches[0].pageX, event.touches[0].pageY);
        }

        function _onMove(moveX, moveY) {
            _move(
                (moveX - _currentMovePoint.x) / _currentZoomValue,
                (moveY - _currentMovePoint.y) / _currentZoomValue
            );

            _currentMovePoint.x = moveX;
            _currentMovePoint.y = moveY;
        }

        function _endMove() {
            $document.off('mousemove', _mouseMove);
            $document[0].removeEventListener('touchmove', _touchMove);
            $document.off('mouseup', _endMove);
            $document.off('touchend', _endMove);
        }

        function _zoomInTouch(event) {
            isTouch = true;
            _zoomIn(event);
        }

        function _zoomInMouse(event) {
            if (!isTouch) {
                _zoomIn(event);
            }
        }

        function _zoomIn(event) {
            event.stopPropagation();

            _setCurrentZoom(_currentZoomIndex + 1);
            $scope.$apply();
        }

        function _zoomOutTouch(event) {
            isTouch = true;
            _zoomOut(event);
        }

        function _zoomOutMouse(event) {
            if (!isTouch) {
                _zoomOut(event);
            }
        }

        function _zoomOut(event) {
            event.stopPropagation();

            _setCurrentZoom(_currentZoomIndex - 1);
            $scope.$apply();
        }

        function _enterKeyDownListener(listener) {
            return function(event) {
                if ((event.which || event.keyCode) === 13) {
                    return listener(event);
                }
            }
        }

        function _setButtonDisabled(buttonElement, isDisabled) {
            if (isDisabled) {
                buttonElement.addClass('disabled');
            } else {
                buttonElement.removeClass('disabled');
            }
        }

        function _setCssValue() {
            var $image = _getImageElement(),
                cssValue = 'scale(' + _currentZoomValue + ') translate(' + _translateLeft + 'px, ' + _translateTop + 'px)';

            $image.css({
                '-webkit-transform': cssValue,
                '-ms-transform': cssValue,
                'transform': cssValue
            });
        }

        function _move(addLeft, addTop) {
            var $image = _getImageElement(),
                actualWidth = $image.prop('offsetWidth') * _currentZoomValue,
                actualHeight = $image.prop('offsetHeight') * _currentZoomValue,
                maxLeft = Math.max(0, (actualWidth - $element.prop('offsetWidth')) / 2 / _currentZoomValue),
                maxTop = Math.max(0, (actualHeight - $element.prop('offsetHeight')) / 2 / _currentZoomValue);

            _translateLeft += addLeft;
            _translateLeft = Math.min(_translateLeft, maxLeft);
            _translateLeft = Math.max(_translateLeft, maxLeft * -1);

            _translateTop += addTop;
            _translateTop = Math.min(_translateTop, maxTop);
            _translateTop = Math.max(_translateTop, maxTop * -1);

            _setCssValue();
        }

        function _getImageElement() {
            return angular.element($element[0].getElementsByClassName('image'));
        }

        function _setCurrentZoom(newIndex) {
            var maxIndex = zoomOptions.length - 1,
                minIndex = 0;

            newIndex = Math.min(maxIndex, newIndex);
            newIndex = Math.max(minIndex, newIndex);

            if (newIndex === _currentZoomIndex) {
                return;
            }

            _currentZoomIndex = newIndex;

            _setButtonDisabled($zoomInButton, _currentZoomIndex === maxIndex);
            _setButtonDisabled($zoomOutButton, _currentZoomIndex === minIndex);

            _currentZoomValue = zoomOptions[_currentZoomIndex];
            if (typeof _currentZoomValue === 'function') {
                _currentZoomValue = _currentZoomValue();
            }

            _setZoomedImageUrl();
            _move(0, 0);
        }

        function _setZoomedImageUrl() {
            if (!options.zoomedImageUrl) {
                return;
            }

            options.SpImagesLoader.load(options.zoomedImageUrl, function(isLoaded, isCache) {
                if (!isLoaded) {
                    return;
                }

                // if (_currentZoomValue > 1) {
                    $scope.calcImageUrl = options.zoomedImageUrl;
                // } else {
                //     delete $scope.calcImageUrl;
                // }

                // apply changes when not a sync call
                if (!isCache) {
                    $scope.$apply();
                }
            });
        }
    }

    app.constant('ZoomControls', ZoomControls);
})(angular, angular.module('spImageZoom'));