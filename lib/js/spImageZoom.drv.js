(function (angular, app) {
    'use strict';

    /**
     * Image zoom directive Link
     * @name ImageZoomLink
     */
    function drvLink($scope, $element, MagnifyingGlass, ZoomControls, SpImagesLoader, IMAGE_ZOOM_MODES, ZOOM_CONTROLS_ZOOM_OPTIONS) {
        var TypesByMode = {};
        TypesByMode[IMAGE_ZOOM_MODES.MAGNIFYING_GLASS] = MagnifyingGlass;
        TypesByMode[IMAGE_ZOOM_MODES.ZOOM_CONTROLS] = ZoomControls;

        var instance;

        $scope.$watch(function() {
            return _getMode() + '-' + $scope.imageUrl + '-' + $scope.zoomedImageUrl + '-' + JSON.stringify(_getZoom() || -1);
        }, _init);

        $scope.$on('$destroy', function() {
            _destroyInstance();
        });

        function _init() {
            _destroyInstance();
            _loadZoomedImage();

            var Type = TypesByMode[_getMode()];
            if (Type) {
                instance = new Type($scope, $element, {
                    imageUrl: $scope.imageUrl,
                    zoomedImageUrl: $scope.zoomedImageUrl,
                    zoom: _getZoom(),
                    SpImagesLoader: SpImagesLoader
                });
            }
        }

        function _getMode() {
            return $scope.mode || IMAGE_ZOOM_MODES.MAGNIFYING_GLASS;
        }

        function _getZoom() {
            var mode = _getMode();
            if (mode === IMAGE_ZOOM_MODES.MAGNIFYING_GLASS) {
                return $scope.glassZoom;
            } else if (mode === IMAGE_ZOOM_MODES.ZOOM_CONTROLS) {
                return $scope.controlsZoom || ZOOM_CONTROLS_ZOOM_OPTIONS;
            }
        }

        function _destroyInstance() {
            if (instance && instance.destroy) {
                instance.destroy();
                instance = undefined;
            }
        }

        function _loadZoomedImage() {
            if ($scope.zoomedImageUrl) {
                SpImagesLoader.load($scope.zoomedImageUrl, function() {});
            }
        }
    }

    /**
     * Register to Module
     */
    app.directive('spImageZoom', [
        'MagnifyingGlass', 'ZoomControls', 'SpImagesLoader', 'IMAGE_ZOOM_MODES', 'ZOOM_CONTROLS_ZOOM_OPTIONS',
        function(MagnifyingGlass, ZoomControls, SpImagesLoader, IMAGE_ZOOM_MODES, ZOOM_CONTROLS_ZOOM_OPTIONS) {
            return {
                restrict: 'A',
                template: '' +
                    '<span class="image-to-middle"></span>' +
                    // calcImageUrl can be calculated and set on the zoom type (i.e. magnifying glass and zoom controls)
                    '<img alt="{{alt}}" class="image" ng-src="{{calcImageUrl || imageUrl}}"/>',
                scope: {
                    imageUrl: '@spImageZoom',
                    zoomedImageUrl: '@zoomedImage',
                    alt: '@?',
                    glassZoom: '<?',
                    controlsZoom: '<?',
                    mode: '<?'
                },
                link: function($scope, $element) {
                  return drvLink($scope, $element, MagnifyingGlass, ZoomControls, SpImagesLoader, IMAGE_ZOOM_MODES, ZOOM_CONTROLS_ZOOM_OPTIONS);
                }
            }
        }
    ]);
})(angular, angular.module('spImageZoom'));
