(function (angular, app) {
    'use strict';

    var DEFAULT_ZOOM = 2;

    function MagnifyingGlass($scope, $element, options) {
        options = options || {};

        var zoom = Number(options.zoom) || DEFAULT_ZOOM,
            $largeImageElement = angular.element('<div class="large-image" style="background-image: url(' + options.imageUrl + ')"></div>'),
            modeClass = 'magnifying-glass-mode';

        $element.addClass(modeClass);
        $element.append($largeImageElement);

        $element.on('mousemove', _mouseMove);
        $element.on('mouseleave', _mouseLeave);

        this.destroy = destroy;

        function destroy() {
            $element.off('mousemove', _mouseMove);
            $element.off('mouseleave', _mouseLeave);
            $element.removeClass(modeClass);
            $largeImageElement.remove();
        }

        function _mouseMove(event) {
            var $smallImage = $element[0].getElementsByClassName('image')[0],
                elementOffset = _getElementOffset($element),
                scrolledElement = elementOffset.hasFixed && _getScrolledElement(),
                pointX = event.pageX + elementOffset.scrollLeft - elementOffset.left - $smallImage.offsetLeft - (scrolledElement ? scrolledElement.scrollLeft : 0),
                pointY = event.pageY + elementOffset.scrollTop - elementOffset.top - $smallImage.offsetTop - (scrolledElement ? scrolledElement.scrollTop : 0);

            if (pointX < $smallImage.offsetWidth && pointY < $smallImage.offsetHeight && pointX > 0 && pointY > 0) {
                $largeImageElement.addClass('shown');
                var positionX = Math.round(pointX * zoom - $largeImageElement[0].offsetWidth / 2) * -1,
                    positionY = Math.round(pointY * zoom - $largeImageElement[0].offsetHeight / 2) * -1,
                    left = pointX + $smallImage.offsetLeft - $largeImageElement[0].offsetWidth / 2,
                    top = pointY + $smallImage.offsetTop - $largeImageElement[0].offsetHeight / 2;
                $largeImageElement.css({
                    'left': left + 'px',
                    'top': top + 'px',
                    'background-position': positionX + 'px ' + positionY + 'px',
                    'background-size': ($smallImage.offsetWidth * zoom) + 'px ' + ($smallImage.offsetHeight * zoom) + 'px'
                });
            } else {
                $largeImageElement.removeClass('shown');
            }
        }

        function _mouseLeave() {
            $largeImageElement.removeClass('shown');
        }
    }

    /**
     * Gets whether the element has fixed position or not
     * @param {HTMLElement} element
     * @returns {boolean}
     * @private
     */
    function _getIsFixedElement(element) {
        var position = '';
        if (window.getComputedStyle && angular.isFunction(window.getComputedStyle)) {
            position = window.getComputedStyle(element).getPropertyValue('position');
        } else if (element.currentStyle && element.currentStyle['position']) {
            position = element.currentStyle['position'];
        }
        return position === 'fixed';
    }

    /**
     * Gets data about the offset of an element
     * @param {HTMLElement} elem
     * @returns {{top: number, left: number, scrollTop: number, scrollLeft: number, hasFixed: boolean}}
     * @private
     */
    function _getElementOffset(elem) {
        elem = angular.element(elem)[0];
        var top = 0,
            left = 0,
            scrollTop = 0,
            scrollLeft = 0,
            hasFixed = false;
        while (elem) {
            hasFixed = hasFixed || _getIsFixedElement(elem);
            top += elem.offsetTop;
            left += elem.offsetLeft;
            if (!hasFixed) {
                scrollTop += elem.scrollTop;
                scrollLeft += elem.scrollLeft;
            }

            elem = elem.offsetParent;
        }
        return {top: top, left: left, scrollTop: scrollTop, scrollLeft: scrollLeft, hasFixed: hasFixed};
    }

    /**
     * Gets the element that has the main scroll event and data
     * @returns {HTMLDocument|HTMLElement}
     * @private
     */
    function _getScrolledElement() {
        var elem = document;
        if (elem.documentElement && elem.documentElement.scrollTop) {
            elem = elem.documentElement;
        } else {
            elem = elem.body || elem;
        }
        return elem;
    }

    app.constant('MagnifyingGlass', MagnifyingGlass);
})(angular, angular.module('spImageZoom'));
