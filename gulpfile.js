'use strict';

var gulp = require('gulp'),
	git = require('gulp-git'),
	sass = require('gulp-sass'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	minifyCss = require('gulp-clean-css'),
	autoPrefixer = require('gulp-autoprefixer'),
	paths = {
		js: ['app.js', 'lib/js/*'],
		sass: 'lib/sass/*'
	};

const dist = gulp.series(_distJs, _distScss);
module.exports = {
	default: dist,
	dist
};

function _distJs() {
	return gulp.src(paths.js)
		.pipe(concat('sp-image-zoom.js'))
		.pipe(gulp.dest('dist'))
		.pipe(git.add())
		.pipe(uglify({
			compress: true
		}))
		.pipe(rename('sp-image-zoom.min.js'))
		.pipe(gulp.dest('dist'))
		.pipe(git.add());
}

function _distScss() {
	return gulp.src(paths.sass)
		.pipe(sass({
			errLogToConsole: true,
			precision: 10
		}))
		.pipe(concat('sp-image-zoom.css'))
		.pipe(autoPrefixer({
			overrideBrowserslist: ['ie 9', 'last 4 versions', 'opera 12.1', 'ios 6', 'android 4', '> 1%']
		}))
		.pipe(gulp.dest('dist'))
		.pipe(git.add())
		.pipe(minifyCss({keepSpecialComments: 0, timeout: 30, processImport: true}))
		.pipe(rename('sp-image-zoom.min.css'))
		.pipe(gulp.dest('dist'))
		.pipe(git.add());
}